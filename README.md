# Global Kinetics Weather Application #

This document outlines the steps necessary to test the Global Kinetics Weather application.

### What is this repository for? ###

This repository and the code contained therein are released as a proof of concept as part of Global Kinetics' coding challenge for candidates to the role of Senior Android Developer. Although both the source code and related resources are open to public access with no explicit licensing restrictions, they remain sole property of Global Kinetics and the Developer.

**Version 1.0.0**

### How do I get set up? ###

Below the steps to test the application:

* Ensure that the device's GPS sensors are off
* Launch the application from the device's home screen
* A short logo animation should play for less than 5 seconds
* The application should prompt you to allow location permissions
* Deny all permissions (for now)
* A bottom popup should appear requesting you to toggle location permissions
* Action the bottom popup's unique button, on the right-hand side
* The application should proceed to its main, "weather" screen
* A native popup should appear requesting you to turn GPS sensors
* You could either choose to action the popup or turn on the GPS sensors from outside the application
* A loading wheel should appear with a somewhat dim background
* The screen's main interface should be populated with weather information for the user's current city/location
* To refresh the information, click the refresh button/icon on the middle right of the screen
* To select a different city, click on the current city appearing at the top
* A search bar should appear with a list of all the world's major cities

### Contribution guidelines ###

* No contribution expected on this repository

### Libraries and tools used ###

* Kotlin
* Depency injection with Koin
* Parsing with Gson
* HTTP/API calls with Retrofit2
* Image loading with Glide

#### For further detail and information, contact [arlon.mukeba@gmail.com](mailto:arlon.mukeba@gmail.com)