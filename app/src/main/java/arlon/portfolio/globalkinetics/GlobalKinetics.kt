package arlon.portfolio.globalkinetics

import android.app.Application
import arlon.portfolio.globalkinetics.model.Coord
import arlon.portfolio.globalkinetics.viewmodel.LocationViewModel
import arlon.portfolio.globalkinetics.viewmodel.repository.WeatherService
import arlon.portfolio.globalkinetics.viewmodel.WeatherViewModel
import arlon.portfolio.globalkinetics.viewmodel.repository.CoordDeserializer
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GlobalKinetics : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@GlobalKinetics)

            modules(
                listOf(
                    appModule
                )
            )
        }
    }

    private val appModule = module {

        single<WeatherService> {
            val customGson = GsonBuilder().registerTypeAdapter(Coord::class.java, CoordDeserializer()).create()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(customGson))
                .build()

            retrofit.create(WeatherService::class.java)
        }

        viewModel { LocationViewModel(baseContext) }
        viewModel { WeatherViewModel(baseContext, get()) }
    }

    companion object {

        const val BASE_URL: String = "https://api.openweathermap.org/data/2.5/"
    }
}