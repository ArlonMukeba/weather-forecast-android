package arlon.portfolio.globalkinetics.model

data class City(val name: String, val lat: Double, val lng: Double, val country: String)