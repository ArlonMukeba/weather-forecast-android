package arlon.portfolio.globalkinetics.model

import com.google.gson.annotations.SerializedName

data class Clouds (

    @SerializedName("all") val all : Int
)