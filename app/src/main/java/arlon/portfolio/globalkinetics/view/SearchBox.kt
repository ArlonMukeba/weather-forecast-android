package arlon.portfolio.globalkinetics.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import arlon.portfolio.globalkinetics.R

open class SearchBox(context: Context, attrs: AttributeSet): ConstraintLayout(context, attrs) {

    open var mListener: SearchBoxListener? = null
    open var mEdtSearch: EditText? = null
    open var mIvSearchCancel: ImageView? = null

    open fun setAttributes(context: Context, attrs: AttributeSet?) {
        val view = inflate(context, R.layout.layout_search_box, this)
        mIvSearchCancel = view.findViewById(R.id.ivCancel)
        (mIvSearchCancel as View).setOnClickListener(ViewClickListener())
        mEdtSearch = view.findViewById(R.id.edtSearch)
        (mEdtSearch as EditText).addTextChangedListener(EditTextListener())
        var attributes: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.SearchBox)
        attributes?.recycle()
        requestLayout()
    }

    fun setSearchListener(listener: SearchBoxListener?) {
        mListener = listener
    }

    interface SearchBoxListener {
        fun textChanged(text: String?)
        fun searchCancel(view: View?)
    }

    private inner class EditTextListener : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable) {
            if (mListener == null) return
            mListener!!.textChanged(s.toString())
        }
    }

    fun clearEditText() {
        mEdtSearch!!.setText("")
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mEdtSearch!!.windowToken, 0)
    }

    fun performCancel() {
        mIvSearchCancel!!.performClick()
    }

    val string: String
        get() = mEdtSearch!!.text.toString()

    private inner class ViewClickListener : OnClickListener {
        @SuppressLint("RestrictedApi")
        override fun onClick(view: View) {
            if (view.id == R.id.ivCancel) {
                clearEditText()
                if (mListener == null) return
                mListener!!.searchCancel(view)
            }
        }
    }

    init {
        setAttributes(context, attrs)
    }
}