package arlon.portfolio.globalkinetics.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import arlon.portfolio.globalkinetics.R
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    companion object {
        private const val PERMISSION_REQUEST = 1928

        private val PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }

    private lateinit var snackBar: Snackbar
    private var firstTime: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_splash)

        snackBar = Snackbar.make(
            clParent,
            R.string.permissions_error_msg,
            Snackbar.LENGTH_INDEFINITE
        )
        animateLogo()
    }

    override fun onResume() {
        super.onResume()
        if (firstTime) return
        checkPermissions()
    }

    private fun animateLogo() {
        val animation = AnimationUtils.loadAnimation(
            this,
            R.anim.logo_anim
        )

        animation.fillAfter = true
        animation.setAnimationListener(AnimationListener())
        ivLogo.startAnimation(animation)
    }

    inner class AnimationListener : Animation.AnimationListener {

        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {
            requestPermissions()
        }

        override fun onAnimationStart(animation: Animation?) {

        }
    }

    fun requestPermissions() {
        firstTime = false

        when {
            checkPermissions() ->
                moveToHome()
            else ->
                ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS,
                    PERMISSION_REQUEST
                )
        }
    }

    private fun checkPermissions(): Boolean = PERMISSIONS.all {
        ActivityCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST && PermissionChecker.PERMISSION_DENIED in grantResults) {
            showPermissionError()
        } else {
            moveToHome()
        }
    }

    private fun showPermissionError() {
        snackBar.let {
            it.setAction(R.string.permissions_allow) { v ->
                it.dismiss()
                requestPermissions()
            }.setBackgroundTint(resources.getColor(R.color.colorPrimary)).setTextColor(
                resources.getColor(R.color.white)
            ).setActionTextColor(
                resources.getColor(R.color.white)
            ).show()
        }
    }

    private fun moveToHome() {
        val runnable = Runnable {
            Intent(this, WeatherActivity::class.java)
                .addFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                .let(this::startActivity)
        }

        Handler().postDelayed(runnable, 1000)
    }
}