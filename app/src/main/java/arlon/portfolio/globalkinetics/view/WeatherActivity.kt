package arlon.portfolio.globalkinetics.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import arlon.portfolio.globalkinetics.R
import arlon.portfolio.globalkinetics.model.City
import arlon.portfolio.globalkinetics.viewmodel.LocationViewModel
import arlon.portfolio.globalkinetics.viewmodel.WeatherViewModel
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_weather.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class WeatherActivity : AppCompatActivity(), SearchBox.SearchBoxListener {

    companion object {
        private const val GPS_ENABLE_REQUEST = 1236
    }

    private lateinit var snackBar: Snackbar
    private val locationViewModel: LocationViewModel by viewModel()
    private val weatherViewModel: WeatherViewModel by viewModel()
    
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_weather)

        ivRefresh.setOnClickListener { getGPSLocation() }
        tvCity.setOnClickListener {
            searchCancel(null)
            clCities.isVisible = true
        }
        sbSearch.setSearchListener(this)
        rvCities.adapter = CityAdapter()
        rvCities.itemAnimator = DefaultItemAnimator()

        observeLocation()
        observeGPSServices()
        observeWeatherData()
        getGPSLocation()
    }

    private fun getGPSLocation() {
        if (!locationViewModel.isGPSEnabled()) {
            locationViewModel.enableGPSServices()
            return
        }

        locationViewModel.getUserLocation()
    }

    private fun observeLocation() {
        locationViewModel.locationRequestStatus.observe(this, { location ->
            location ?: return@observe

            if (location is String && location == LocationViewModel.LOCATION_REQUEST_LOADING) {
                clLoading.isVisible = true
                return@observe
            }

            with (location as City) {
                getWeather(this)
            }
        }
        )
    }

    private fun getWeather(city: City) {
        clCities.isVisible = false
        clLoading.isVisible = true
        sbSearch.clearEditText()
        weatherViewModel.getWeather(city)
    }

    private fun observeGPSServices() {
        locationViewModel.gpsRequestStatus.observe(this, { status ->
            when {
                status is String && status == LocationViewModel.GPS_REQUEST_FAILED -> {
                    showGPSError()
                }
                status is ResolvableApiException -> {
                    status.startResolutionForResult(
                        this@WeatherActivity, GPS_ENABLE_REQUEST
                    )
                }
            }
        }
        )
    }

    private fun observeWeatherData() {
        weatherViewModel.weatherData.observe(this, { weatherData ->
            weatherData?.let { payload ->
                tvCity.text = payload.name
                tvDate.text = SimpleDateFormat("EEE, dd MMMM HH:mm").format(Date())
                Glide.with(this).load(
                    String.format(
                        getString(R.string.weather_image_url),
                        payload.weather[0].icon
                    )
                ).into(ivSign)
                tvTemp.text = "${payload.main.temp.toInt()}°"
                tvTempVariant.text =
                    "${payload.main.temp_max.toInt()}°/${payload.main.temp_min.toInt()}° Feels like ${payload.main.feels_like.toInt()}°"
                tvMood.text =
                    payload.weather[0].description.split(" ").joinToString(" ") { it.capitalize() }
                        .trimEnd()
                tvSunrise.text = SimpleDateFormat("HH:mm").format(Date(payload.sys.sunrise * 1000L))
                tvSunset.text = SimpleDateFormat("HH:mm").format(Date(payload.sys.sunset * 1000L))
                tvHumidity.text = "${payload.main.humidity}"
                tvPressure.text = "${payload.main.pressure}"
                tvWind.text = "${payload.wind.speed}"
                tvVisibility.text = "${payload.visibility}"
                clLoading.isVisible = false
                tvCity.isClickable = true
            }
        }
        )
    }

    private fun showGPSError() {
        snackBar.apply {
            setAction(R.string.gps_enable) {
                dismiss()
                locationViewModel.enableGPSServices()
            }.setBackgroundTint(resources.getColor(R.color.colorPrimary)).setTextColor(
                resources.getColor(R.color.white)
            ).setActionTextColor(
                resources.getColor(R.color.colorAccent)
            ).show()
        }
    }

    override
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GPS_ENABLE_REQUEST && resultCode == RESULT_OK) {
            locationViewModel.getUserLocation()
            return
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (clCities.isVisible) {
            clCities.isVisible = false
            sbSearch.clearEditText()
            return
        }

        super.onBackPressed()
    }

    override fun textChanged(text: String?) {
        (rvCities.adapter as CityAdapter).filter.filter(text)
    }

    override fun searchCancel(view: View?) {
        (rvCities.adapter as CityAdapter).filter.filter("")
    }

    inner class CityAdapter : RecyclerView.Adapter<CityViewHolder>(), Filterable {

        private var mCityList = weatherViewModel.getCities()
        private var mFilteredCityList = mutableListOf<City>()

        override
        fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : CityViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                android.R.layout.simple_list_item_1, null
            )

            return CityViewHolder(view)
        }

        open override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
            holder.update(mFilteredCityList[position])
        }

        open override fun getItemCount() : Int =
            5.coerceAtMost(mFilteredCityList.size).coerceAtLeast(mFilteredCityList.size)

        override fun getFilter() = CityFilter()

        inner class CityFilter: Filter() {
            override
            fun performFiltering(constraint: CharSequence): FilterResults {
                var cities: MutableList<City> = mCityList.toMutableList()
                var filterResults = FilterResults()
                cities.filter { city: City -> city.name.contains(constraint,
                    true) }.let {
                    filterResults.values = it
                    filterResults.count = it.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults) {
                mFilteredCityList.clear()
                mFilteredCityList.addAll(results.values as List<City>)

                notifyDataSetChanged()
            }
        }
    }

    inner class CityViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun update(city: City) {
            (itemView as TextView).setTextColor(itemView.context.resources
                .getColor(R.color.colorPrimary))

            val colorSpan = ForegroundColorSpan(itemView.context.resources.getColor(
                R.color.colorAccent))

            val searchString = sbSearch.string
            val index = city.name.toLowerCase().indexOf(searchString.toLowerCase())
            val ssb = SpannableStringBuilder("${city.name}, ${city.country}")

            if (index != -1) {
                ssb.setSpan(colorSpan, index, (index + searchString.length),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }

            (itemView as TextView).text = ssb
            itemView.setOnClickListener { getWeather(city) }
        }
    }
}