package arlon.portfolio.globalkinetics.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Looper
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arlon.portfolio.globalkinetics.model.City
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import org.koin.core.KoinComponent
import java.util.*


class LocationViewModel(
    private val context: Context
) : ViewModel(), KoinComponent,
    OnSuccessListener<LocationSettingsResponse>, OnFailureListener {

    var city: City? = null
    private val locationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    var locationRequestStatus: MutableLiveData<Any?> = MutableLiveData()
    var gpsRequestStatus: MutableLiveData<Any> = MutableLiveData()

    private val locationRequest: LocationRequest? = LocationRequest.create()?.apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult?.locations?.firstOrNull()?.let {
                val details = getSpecifics(it)
                locationRequestStatus.value = City(details[0], it.latitude, it.longitude, details[1])
            }

            locationClient.removeLocationUpdates(this)
        }
    }

    private fun getSpecifics(location: Location): List<String> {
        val geocoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address> = geocoder.getFromLocation(location.latitude, location.longitude, 1)
        return listOf<String>(addresses[0].locality, addresses[0].countryName)
    }

    fun isGPSEnabled(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            (context.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager)
                .isLocationEnabled
        } else {
            Settings.Secure.getInt(
                context.contentResolver, Settings.Secure.LOCATION_MODE,
                Settings.Secure.LOCATION_MODE_OFF
            ) != Settings.Secure.LOCATION_MODE_OFF
        }
    }

    fun enableGPSServices() {
        locationRequest?.let {
            val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
            val client: SettingsClient = LocationServices.getSettingsClient(context)
            val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
            task.addOnSuccessListener(this)
            task.addOnFailureListener(this)
        }
    }

    @SuppressLint("MissingPermission")
    fun getUserLocation() {
        locationRequestStatus.value = LOCATION_REQUEST_LOADING

        locationClient.requestLocationUpdates(
            locationRequest,
            locationCallback, Looper.getMainLooper()
        )
    }

    override fun onSuccess(response: LocationSettingsResponse?) {
        getUserLocation()
    }

    override fun onFailure(exception: Exception) {
        when (exception) {
            is ResolvableApiException -> gpsRequestStatus.postValue(exception)
            else -> gpsRequestStatus.postValue(GPS_REQUEST_FAILED)
        }
    }

    companion object {
        const val GPS_REQUEST_FAILED: String = "GPS_REQUEST_FAILED"
        const val LOCATION_REQUEST_LOADING: String = "LOCATION_REQUEST_LOADING"
    }
}