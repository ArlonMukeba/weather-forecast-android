package arlon.portfolio.globalkinetics.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arlon.portfolio.globalkinetics.R
import arlon.portfolio.globalkinetics.model.City
import arlon.portfolio.globalkinetics.model.Payload
import arlon.portfolio.globalkinetics.viewmodel.repository.CityDeserializer
import arlon.portfolio.globalkinetics.viewmodel.repository.WeatherService
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import org.koin.core.KoinComponent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class WeatherViewModel(
    private val context: Context,
    private val weatherService: WeatherService
) : ViewModel(), KoinComponent {

    var weatherData: MutableLiveData<Payload> = MutableLiveData()
    var weatherRequestError: MutableLiveData<String> = MutableLiveData()

    fun getWeather(city: City) {
        weatherService.run {
                fetchWeather(city.lat.toDouble(), city.lng.toDouble()).enqueue(object :
                    Callback<Payload> {

                    override fun onResponse(call: Call<Payload>, response: Response<Payload>) {
                        response.errorBody()?.let {
                            weatherRequestError.postValue(it.string())
                        }

                        response.body()?.let { payload ->
                            weatherData.value = payload
                        }
                    }

                    override fun onFailure(call: Call<Payload>, t: Throwable) {
                        weatherRequestError.postValue(t.message)
                    }
                }
                )
        }
    }

    fun getCities(): List<City> {
        val gsonBuilder = GsonBuilder().apply {
            registerTypeAdapter(City::class.java, CityDeserializer())
        }

        return (gsonBuilder.create().fromJson(
            readJSONContent(context, R.raw.cities).toString(),
            CityList::class.java
        )).cities.sortedBy { city: City -> city.name}
    }

    private fun readJSONContent(context: Context, jsonFileResId: Int): JSONObject? {
        val inputStream = context.resources.openRawResource(jsonFileResId)
        val inputString = Scanner(inputStream).useDelimiter("\\A").next()
        return getObject(inputString)
    }

    private fun getObject(jsonString: String): JSONObject? {
        return try {
            JSONObject(jsonString)
        } catch (e: java.lang.Exception) {
            JSONObject()
        }
    }

    data class CityList(
        @SerializedName("cities") val cities: List<City>
    )
}