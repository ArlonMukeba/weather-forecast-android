package arlon.portfolio.globalkinetics.viewmodel.repository

import arlon.portfolio.globalkinetics.model.City
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class CityDeserializer: JsonDeserializer<City> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): City {
        var name = ""
        var lat = 0.0
        var lon = 0.0
        var country = ""

        json?.asJsonObject?.let {
            name = it.get("name").asString
            lat = it.get("lat").asString.replace(',', '.').toDouble()
            lon = it.get("lng").asString.replace(',', '.').toDouble()
            country = it.get("country").asString
        }

        return City(name, lat, lon, country)
    }
}