package arlon.portfolio.globalkinetics.viewmodel.repository

import arlon.portfolio.globalkinetics.model.Coord
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class CoordDeserializer: JsonDeserializer<Coord> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Coord {
        val jsonObject = json?.asJsonObject
        var lat = -33.0
        var lon = 18.0

        jsonObject?.let {
            lat = it.get("lat").asDouble
            lon = it.get("lon").asDouble
        }

        return Coord(lon, lat)
    }
}