package arlon.portfolio.globalkinetics.viewmodel.repository

import arlon.portfolio.globalkinetics.model.Payload
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("weather")
    fun fetchWeather(
        @Query(value = "lat") lat: Double,
        @Query(value = "lon") lon: Double,
        @Query(value = "units") units: String = "metric",
        @Query(value = "appid") appId: String = "53f9d8e4213222cf517d86dc406d67fc"
    ): Call<Payload>
}